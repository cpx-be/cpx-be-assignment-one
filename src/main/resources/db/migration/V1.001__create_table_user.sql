create table app_user (
  id             serial primary key,
  first_name     varchar(100),
  last_name      varchar(100),
  email          varchar(100),
  user_type      varchar(100),
  create_by      varchar(50),
  create_date    date ,
  modify_by      varchar(50),
  modify_date    date,
  employee_id    varchar(20)
);