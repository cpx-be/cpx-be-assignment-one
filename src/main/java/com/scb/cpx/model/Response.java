package com.scb.cpx.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.Map;

public class Response implements Serializable {

    private Integer status;
    private String message;

    @JsonProperty("result_error")
    private Map<String, String> resultError;

    private Object data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status.value();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Map<String, String> getResultError() {
        return resultError;
    }

    public void setResultError(Map<String, String> resultError) {
        this.resultError = resultError;
    }
}
