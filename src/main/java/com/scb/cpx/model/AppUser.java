package com.scb.cpx.model;


import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AppUser extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotEmpty //(message = "Cannot be null")
    private String firstName;

    @NotEmpty //(message = "Cannot be null")
    private String lastName;

    @Email //(message = "Should be valid")
    private String email;

    private String userType;
    private String employeeId;
}
