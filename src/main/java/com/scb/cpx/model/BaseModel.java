package com.scb.cpx.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigInteger;
import java.time.LocalDateTime;

@Data
public class BaseModel {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected Integer id;


    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected String createBy;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected String modifyBy;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    protected LocalDateTime createDate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    protected LocalDateTime modifyDate;

    protected String createByName;
    protected String modifyByName;
    protected String createByRole;
    protected String modifyByRole;
}
