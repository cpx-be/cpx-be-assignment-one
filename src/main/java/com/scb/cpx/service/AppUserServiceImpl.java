package com.scb.cpx.service;

import com.scb.cpx.dao.AppUserDao;
import com.scb.cpx.exception.DataBaseException;
import com.scb.cpx.exception.NotFoundException;
import com.scb.cpx.model.AppUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class AppUserServiceImpl implements AppUserService {

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AppUserDao appUserDao;

    @Override
    public Integer create(AppUser appUser) throws DataBaseException {
        log.debug("Start create {}.", appUser.getId());
        appUser.setCreateBy("Admin");
//        if(appUser.getPassword()!=null && !appUser.getPassword().isEmpty()) {
//            String encryptedPassword = new BCryptPasswordEncoder().encode(appUser.getPassword());
//            appUser.setPassword(encryptedPassword);
//        }
        Integer id = appUserDao.create(appUser);
        log.debug("End create {}.", appUser.getId());

        return id;
    }
    @Override
    public AppUser findById(int id) throws NotFoundException {
        log.info("Start findById {}.", id);
        AppUser appUser = appUserDao.findById(id);
        log.debug("End findById {}.", id);
        return appUser;
    }

    @Override
    public List<AppUser> list() {
        log.debug("Start findAll ");
        List<AppUser> result = appUserDao.list()
                .stream()
                //.peek(appUser -> appUser.setPassword(""))
                .collect(Collectors.toList());

        log.debug("End findAll ");

        return result;
    }

    @Override
    public int update(int id, AppUser appUser) throws NotFoundException, DataBaseException {
        log.debug("Start update {}.", id);
        this.findById(id);
        appUser.setId(id);
        appUser.setModifyBy("Admin");
        int newId = appUserDao.update(id,appUser);
        log.debug("End update {}.", id);
        return newId;
    }

    @Override
    public int delete(int id) throws NotFoundException{
        log.debug("Start delete {}.", id);
        this.findById(id);
        appUserDao.delete(id);
        log.debug("End delete {}.", id);
        return id;
    }
}
