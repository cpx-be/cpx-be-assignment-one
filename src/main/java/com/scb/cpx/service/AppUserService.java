package com.scb.cpx.service;

import com.scb.cpx.exception.DataBaseException;
import com.scb.cpx.exception.NotFoundException;
import com.scb.cpx.exception.ValidationException;
import com.scb.cpx.model.AppUser;

import java.math.BigInteger;
import java.util.List;

public interface AppUserService {
    Integer create(AppUser user) throws DataBaseException;
    AppUser findById(int id)throws NotFoundException;
    List<AppUser> list() throws NotFoundException;
    int update(int id, AppUser user) throws NotFoundException, ValidationException, DataBaseException;
    int delete(int id) throws NotFoundException;
}

