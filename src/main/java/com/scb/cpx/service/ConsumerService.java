package com.scb.cpx.service;

import com.scb.cpx.common.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ConsumerService {

	private static final Logger logger = LogManager.getFormatterLogger(ConsumerService.class);

	@KafkaListener(topics = Constants.TOPIC, groupId =  Constants.GROUP_ID)
	public void consume(String message) throws IOException {
		logger.info("Consume Message: %s", message);
	}
}
