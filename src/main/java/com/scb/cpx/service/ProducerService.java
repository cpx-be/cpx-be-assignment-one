package com.scb.cpx.service;

import com.scb.cpx.common.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
public class ProducerService {

	private static final Logger logger = LogManager.getFormatterLogger(ProducerService.class);

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	public ListenableFuture<SendResult<String, String>>  produce(String data) {

		logger.info("Produce Topic: %s - Message: %s", Constants.TOPIC, data);
		ListenableFuture<SendResult<String, String>> future = this.kafkaTemplate.send(Constants.TOPIC, data);
		// register a callback with the listener to receive the result of the send asynchronously
		/*future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

			@Override
			public void onSuccess(SendResult<String, String> result) {
				logger.info("Kafka sent message='{}' with offset={}", data,
						result.getRecordMetadata().offset());
			}

			@Override
			public void onFailure(Throwable ex) {
				logger.error("Kafka unable to send message='{}'", data, ex);
			}
		});*/
		return future;
	}

}
