package com.scb.cpx.common;

public class Constants {

    public static final String RESPONSE_STATUS_ERROR = "1";
    public static final String RESPONSE_STATUS_SUCCESS = "0";
    public static final String RESPONSE_MESSAGE_CODE = "message_code";
    public static final String RESPONSE_MESSAGE_DESC = "message_desc";
    public static final String RESPONSE_MESSAGE_CLASS = "message_detail";
    public static final String TARGET_METHOD = "targetMethod";
    public static final String ELAPSED_TIME = "elapsed";
    public static final String TOPIC = "test-kafka";
    public static final String GROUP_ID = "myGroup";

    public interface MESSAGE_RESPONSE {
        String MSG_CODE_200 = "The request has succeeded.";
        String MSG_CODE_404 = "Resource not found.";
        String MSG_CODE_400 = "Bad Request.";
        String MSG_CODE_401 = "Unauthorized";

    }


    public enum TableName {
        APP_USER
    }

}
