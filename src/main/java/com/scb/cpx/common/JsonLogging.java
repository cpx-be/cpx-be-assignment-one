package com.scb.cpx.common;

import org.springframework.stereotype.Component;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JsonLogging {

    private static final Integer ERR_LINE_PRESENT = 2;

    public static final String DATE_TIME_FMT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    public Map<String, Object> setPayloadWithAction(String action, Object payload) {

        Map<String, Object> response = new HashMap<>();
        if (payload != null) {
            response.put("payload", payload);
        }

        response.put("Action", action);

        return response;
    }

    public Map<Object, Object> setLog(String action, Object msg)
    {
        Map<Object, Object> map = new HashMap<Object,Object>();
        map.put("Action",action);
        map.put("msg",msg);

        return map;
    }


    public Map<Object, Object> setLog(String action, Object msg, String startTime,String tracking, Long elapsed)
    {
        Map<Object, Object> map = new HashMap<Object,Object>();
        map.put("Action",action);
        if(null != msg)
        {
            map.put("msg",msg);
        }

        map.put("StartTime",startTime);
        if(null != tracking)
        {
            map.put("TrackingId",tracking);
        }
        map.put("Elapsed",elapsed);

        return map;
    }

    public Map<Object, Object> setErrorLog(String action, String startTime, Object error)
    {
        Map<Object, Object> map = new HashMap<Object,Object>();
        map.put("Action",action);
        map.put("StartTime",startTime);
        map.put("Error",error);

        return map;
    }


    public String getNewDate()
    {
        DateFormat df = new SimpleDateFormat(DATE_TIME_FMT);
        //df.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
        return df.format(new Date());
    }

}
