package com.scb.cpx.common;

import org.springframework.util.ObjectUtils;

public class StringUtil {

    public static boolean isNullOrEmpty(String str) {
        if(str != null && !str.isEmpty())
            return false;
        return true;
    }

    public static String getArray2String(String[] str) {
        if(ObjectUtils.isEmpty(str)) {
            str = new String[]{};
        }
        return String.join(", ", str);
    }
}
