package com.scb.cpx.common;

import org.springframework.util.ObjectUtils;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class TimeUtil {

    public static Timestamp toTimestamp(LocalDateTime lct) {
        if(ObjectUtils.isEmpty(lct)) {
            return null;
        }

        return Timestamp.valueOf(lct);
    }
}
