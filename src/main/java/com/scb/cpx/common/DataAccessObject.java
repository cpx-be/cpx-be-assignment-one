package com.scb.cpx.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public abstract class DataAccessObject {

    @Autowired
    @Lazy
    protected JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall simpleJdbcCall;

    protected <T extends Object> List<T> query(Class<T> t, String sql, List<Object> paramList) {
        return jdbcTemplate.query(sql, paramList.toArray(new Object[paramList.size()]), new BeanPropertyRowMapper<T>(t));
    }

    protected <T extends Object> List<T> query(Class<T> t, String sql, MapSqlParameterSource params) {
        return new NamedParameterJdbcTemplate(jdbcTemplate).query(sql, params, new BeanPropertyRowMapper<T>(t));
    }

    protected <T extends Object> T queryForObject(Class<T> t, String sql, List<Object> paramList) {
        return jdbcTemplate.queryForObject(sql, paramList.toArray(new Object[paramList.size()]), new BeanPropertyRowMapper<T>(t));
    }

    protected <T extends Object> T queryForObject(Class<T> t, String sql, MapSqlParameterSource params) {
        return new NamedParameterJdbcTemplate(jdbcTemplate).queryForObject(sql, params, new BeanPropertyRowMapper<T>(t));
    }

    protected Integer queryForInteger(String sql, List<Object> paramList) {
        return jdbcTemplate.queryForObject(sql, paramList.toArray(new Object[paramList.size()]), Integer.class);
    }

    protected int save(String sql, List<Object> paramList) {
        return jdbcTemplate.update(sql, paramList.toArray(new Object[paramList.size()]));
    }

    protected int save(String sql, MapSqlParameterSource params) {
        return new NamedParameterJdbcTemplate(jdbcTemplate).update(sql, params);
    }

    protected Map execProd(String prodName, MapSqlParameterSource params){
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName(prodName);
        return simpleJdbcCall.execute(params);
    }

    protected <T> T execProc(Class<T> clazz, String prodName, MapSqlParameterSource params){
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName(prodName);
        return simpleJdbcCall.executeObject(clazz, params);
    }

    protected Map execProdRefCursor(String prodName, MapSqlParameterSource params, String resultSet, Class<?> cls){
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName(prodName)
                .returningResultSet(resultSet, BeanPropertyRowMapper.newInstance(cls));
        return simpleJdbcCall.execute(params);
    }

    protected int[] batchUpdate(String sql,List<Object[]> paramList) {
        int[] updateCounts = jdbcTemplate.batchUpdate(sql,paramList);
        return updateCounts;
    }
    protected int[] batchUpdate(String sql,BatchPreparedStatementSetter ps) {
        int[] updateCounts = jdbcTemplate.batchUpdate(sql, ps);
        return updateCounts;
    }

}
