package com.scb.cpx.exception;

public abstract class AbstractException extends Exception {
    public abstract String getResponseMessage();
}
