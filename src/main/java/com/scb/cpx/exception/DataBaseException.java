package com.scb.cpx.exception;

import com.scb.cpx.common.Constants;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class DataBaseException extends AbstractException {
    private List<String> errorString = new ArrayList<>();

    public DataBaseException(String errorString) {
        this.errorString = Collections.singletonList(errorString);
    }

    @Override
    public String getResponseMessage() {
        return String.join(" ", Constants.MESSAGE_RESPONSE.MSG_CODE_400, getErrorString());
    }

    private String getErrorString() {
        if(ObjectUtils.isEmpty(errorString)) {
            return "";
        }
        return errorString.stream().map(String::toString).collect(Collectors.joining(", "));
    }
}
