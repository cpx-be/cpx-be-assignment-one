package com.scb.cpx.exception;

import com.scb.cpx.common.Constants;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class NotFoundException extends AbstractException {
    private List<BigInteger> ids = new ArrayList<>();

    public NotFoundException(BigInteger id) {
        this.ids = Collections.singletonList(id);
    }

    @Override
    public String getResponseMessage() {
        return String.join(" ", Constants.MESSAGE_RESPONSE.MSG_CODE_404, getIdString());
    }

    private String getIdString() {
        if(ObjectUtils.isEmpty(ids)) {
            return "";
        }
        return ids.stream().map(BigInteger::toString).collect(Collectors.joining(", "));
    }
}
