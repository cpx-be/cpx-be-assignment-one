package com.scb.cpx.exception;

import com.scb.cpx.common.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class ValidationException extends AbstractException {

    private List<Map<String, String>> fieldErrors = new ArrayList<>();

    @Override
    public String getResponseMessage() {
        return Constants.MESSAGE_RESPONSE.MSG_CODE_400;
    }

    public void addFieldError(String fieldName, String description) {
        Map<String, String> error = new HashMap<>();
        error.put(fieldName, description);
        this.fieldErrors.add(error);
    }
}
