package com.scb.cpx.dao;

import com.scb.cpx.exception.DataBaseException;
import com.scb.cpx.exception.NotFoundException;
import com.scb.cpx.model.AppUser;

import java.util.List;

public interface AppUserDao {
    Integer create(AppUser user) throws DataBaseException;
    AppUser findById(int id) throws NotFoundException;
    List<AppUser> list() ;
    int update(int id, AppUser user) throws DataBaseException;
    void delete(int id) ;
}
