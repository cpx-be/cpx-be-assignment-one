package com.scb.cpx.dao;


import com.scb.cpx.common.Constants;
import com.scb.cpx.common.DataAccessObject;
import com.scb.cpx.exception.DataBaseException;
import com.scb.cpx.exception.NotFoundException;
import com.scb.cpx.model.AppUser;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;


@Repository
@Lazy
public class AppUserDaoImpl extends DataAccessObject implements AppUserDao {
    @Override
    public Integer create(AppUser appUser) throws DataBaseException {
        List<Object> paramList = new ArrayList<Object>();
        StringBuilder sb = new StringBuilder();
        try {
            sb.append(" INSERT INTO " + Constants.TableName.APP_USER + " ( " +
                    // " USERNAME, " +
                    // " PASSWORD, " +
                    " FIRST_NAME, " +
                    " LAST_NAME, " +
                    " EMAIL, " +
                    " USER_TYPE, " +
                    " CREATE_BY, " +
                    " CREATE_DATE, " +
                    " EMPLOYEE_ID) " +
                    "VALUES ( " +
                    //  " ?, " +
                    //  " ?, " +
                    " ?, " +
                    " ?, " +
                    " ?, " +
                    " ?, " +
                    " ?, " +
                    " CURRENT_TIMESTAMP, " +
                    " ?) ");

            String sql = sb.toString();

            // this is the key holder
            GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

            // the name of the generated column (you can track more than one column)
            String id_column = "id";

            // the update method takes an implementation of PreparedStatementCreator which could be a lambda
            jdbcTemplate.update(con -> {
                        PreparedStatement ps = con.prepareStatement(sql, new String[]{id_column});
                        // ps.setString(1, appUser.getUsername());
                        // ps.setString(2, appUser.getPassword());
                        ps.setString(1, appUser.getFirstName());
                        ps.setString(2, appUser.getLastName());
                        ps.setString(3, appUser.getEmail());
                        ps.setString(4, appUser.getUserType());
                        ps.setString(5, appUser.getCreateBy());
                        ps.setString(6, appUser.getEmployeeId());
                        return ps;
                    }
                    , keyHolder);

            // after the update executed we can now get the value of the generated ID
            return (Integer) keyHolder.getKeys().get(id_column);
        } catch (Exception e) {
            //e.printStackTrace();
            throw new DataBaseException(e.getMessage());
        }
    }

    @Override
    public AppUser findById(int id) throws NotFoundException {
        try {
            String sql = " SELECT  " +
                    " ID, " +
                    // " USERNAME, " +
                    // " PASSWORD, " +
                    " FIRST_NAME, " +
                    " LAST_NAME, " +
                    " EMAIL, " +
                    " USER_TYPE, " +
                    " CREATE_BY, " +
                    " CREATE_DATE, " +
                    " MODIFY_BY, " +
                    " MODIFY_DATE, " +
                    " EMPLOYEE_ID " +
                    "FROM " + Constants.TableName.APP_USER + " WHERE ID = :id ";
            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("id", id);

            return super.queryForObject(AppUser.class, sql, mapSqlParameterSource);

        } catch (EmptyResultDataAccessException e) {
            throw new NotFoundException(BigInteger.valueOf(id));
        }
    }

    @Override
    public List<AppUser> list() {
        String sql = " SELECT  " +
                " ID, " +
                //  " USERNAME, " +
                //  " PASSWORD, " +
                " FIRST_NAME, " +
                " LAST_NAME, " +
                " EMAIL, " +
                " USER_TYPE, " +
                " CREATE_BY, " +
                " CREATE_DATE, " +
                " MODIFY_BY, " +
                " MODIFY_DATE, " +
                " EMPLOYEE_ID " +
                "FROM APP_USER;";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        return super.query(AppUser.class, sql, mapSqlParameterSource);
    }

    @Override
    public int update(int id, AppUser appUser) throws DataBaseException {
        try {
            String sql = "UPDATE " +
                    "   " + Constants.TableName.APP_USER + " " +
                    "SET " +
                    //  "   USERNAME = :username, " +
                    //  "   PASSWORD = :password, " +
                    "   FIRST_NAME = :firstName, " +
                    "   LAST_NAME = :lastName, " +
                    "   EMAIL = :email, " +
                    "   USER_TYPE = :userType, " +
                    "   EMPLOYEE_ID = :employeeId, " +
                    "   MODIFY_BY = :modifyBy, " +
                    "   MODIFY_DATE = CURRENT_TIMESTAMP " +
                    "WHERE " +
                    "   ID = :id ";

            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource()
                    .addValue("id", appUser.getId())
                    // .addValue("username", appUser.getUsername())
                    // .addValue("password", appUser.getPassword())
                    .addValue("firstName", appUser.getFirstName())
                    .addValue("lastName", appUser.getLastName())
                    .addValue("email", appUser.getEmail())
                    .addValue("userType", appUser.getUserType())
                    .addValue("employeeId", appUser.getEmployeeId())
                    .addValue("modifyBy", appUser.getModifyBy());
            super.save(sql, mapSqlParameterSource);
            return appUser.getId();
        } catch (Exception e) {
            throw new DataBaseException(e.getMessage());
        }
    }

    @Override
    public void delete(int id) {
        String sql = " DELETE FROM " + Constants.TableName.APP_USER + " WHERE ID = :id";

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);
        super.save(sql, mapSqlParameterSource);
    }
}
