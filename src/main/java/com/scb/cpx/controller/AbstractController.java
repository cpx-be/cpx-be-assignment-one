package com.scb.cpx.controller;

import com.scb.cpx.common.Constants;
import com.scb.cpx.exception.DataBaseException;
import com.scb.cpx.exception.NotFoundException;
import com.scb.cpx.exception.ValidationException;
import com.scb.cpx.model.Response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class AbstractController {

    protected ResponseEntity<Response> ok(Object data) {
        Response response = new Response();
        response.setStatus(HttpStatus.OK);
        response.setData(data);
        response.setMessage(Constants.MESSAGE_RESPONSE.MSG_CODE_200);
        return ResponseEntity.ok(response);
    }

    protected ResponseEntity<Response> notFound() {
        Response responseError = new Response();
        responseError.setMessage(Constants.MESSAGE_RESPONSE.MSG_CODE_404);
        responseError.setStatus(HttpStatus.NOT_FOUND);

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(responseError);
    }

    protected ResponseEntity<Response> notFound(NotFoundException e) {
        Response responseError = new Response();
        responseError.setMessage(e.getResponseMessage());
        responseError.setStatus(HttpStatus.NOT_FOUND);

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(responseError);
    }

    protected ResponseEntity<Response> badRequest() {
        Response responseError = new Response();
        responseError.setMessage(Constants.MESSAGE_RESPONSE.MSG_CODE_400);
        responseError.setStatus(HttpStatus.BAD_REQUEST);

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(responseError);
    }

    protected ResponseEntity<Response> badRequest(ValidationException e) {
        Response responseError = new Response();
        responseError.setMessage(e.getResponseMessage());
        responseError.setData(e.getFieldErrors());
        responseError.setStatus(HttpStatus.BAD_REQUEST);

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(responseError);
    }

    protected ResponseEntity<Response> badRequest(DataBaseException e) {
        Response responseError = new Response();
        responseError.setMessage(e.getResponseMessage());
        responseError.setStatus(HttpStatus.BAD_REQUEST);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(responseError);
    }

    protected ResponseEntity<Response> unauthorized() {
        Response responseError = new Response();
        responseError.setMessage(Constants.MESSAGE_RESPONSE.MSG_CODE_401);
        responseError.setStatus(HttpStatus.UNAUTHORIZED);

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(responseError);
    }

}
