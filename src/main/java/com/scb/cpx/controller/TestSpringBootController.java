package com.scb.cpx.controller;

import com.scb.cpx.model.Response;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/be-hello")
public class TestSpringBootController extends AbstractController {
    Logger log = LoggerFactory.getLogger(this.getClass());
    @GetMapping
    public ResponseEntity<Response> writeMsg() {
        log.info("Hello Backend Team");
        return super.ok("Hello Backend Team");
    }
}
