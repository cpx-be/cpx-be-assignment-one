package com.scb.cpx.controller;

import com.scb.cpx.model.Response;
import com.scb.cpx.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/test-kafka")
public class TestKafkaController extends AbstractController {

    @Autowired
    private ProducerService producerService;

    @PostMapping
    public ResponseEntity<Response> sendMessageToKafkaTopic(@RequestBody String msg) {
        producerService.produce(msg);
        return super.ok(msg);
    }
}
