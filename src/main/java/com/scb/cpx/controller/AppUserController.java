package com.scb.cpx.controller;

import com.scb.cpx.exception.DataBaseException;
import com.scb.cpx.exception.NotFoundException;
import com.scb.cpx.exception.ValidationException;
import com.scb.cpx.model.AppUser;
import com.scb.cpx.model.AppUserDTO;
import com.scb.cpx.model.AppUserMapper;
import com.scb.cpx.model.Response;
import com.scb.cpx.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/app-users")
public class AppUserController extends AbstractController {

    @Autowired
    private AppUserService appUserService;

    private AppUserMapper appUserMapper;

    private Validator validator;

    @InitBinder
    private void initial() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @PostMapping
    public ResponseEntity<Response> create(@Valid @RequestBody AppUser appUser, BindingResult result) {
        ValidationException ex = new ValidationException();
        try {
            if (result.hasErrors()) {
                Set<ConstraintViolation<AppUser>> constraintViolations =
                        validator.validate(appUser);
                for (ConstraintViolation<AppUser> violation : constraintViolations) {
                    String propertyPath = violation.getPropertyPath().toString();
                    String message = violation.getMessage();
                    //System.out.println("Invalid "+ propertyPath + " (" + message + ")");
                    ex.addFieldError(propertyPath, message);
                }
                return super.badRequest(ex);
            } else {

                Integer id = appUserService.create(appUser);
                return super.ok(id);
            }
        } catch (DataBaseException e) {
            return super.badRequest(e);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Response> getById(@PathVariable("id") int id) {
        try {
            AppUser appUser = appUserService.findById(id);
            return super.ok(appUser);
        } catch (NotFoundException e) {
            return super.notFound(e);

        }
    }

    @GetMapping("/list")
    public ResponseEntity<Response> list() {
        try {
            List<AppUser> list = appUserService.list();
            return super.ok(list);
        } catch (NotFoundException e) {
            return super.notFound(e);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Response> update(@Valid @PathVariable("id") int id, @Valid @RequestBody AppUser appUser, BindingResult result) {
        ValidationException ex = new ValidationException();
        try {
            if (result.hasErrors()) {
                Set<ConstraintViolation<AppUser>> constraintViolations =
                        validator.validate(appUser);
                for (ConstraintViolation<AppUser> violation : constraintViolations) {
                    String propertyPath = violation.getPropertyPath().toString();
                    String message = violation.getMessage();
                    //System.out.println("Invalid "+ propertyPath + " (" + message + ")");
                    ex.addFieldError(propertyPath, message);
                }
                return super.badRequest(ex);
            } else {
                int newId = appUserService.update(id, appUser);
                return super.ok(newId);
            }
        } catch (NotFoundException e) {
            return super.notFound(e);
        } catch (ValidationException e) {
            return super.badRequest(e);
        } catch (DataBaseException e) {
            return super.badRequest(e);
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Response> partialUpdate(@PathVariable("id") int id, @RequestBody AppUserDTO userDTO) {
        int newId = 0;
        try {
            AppUser user = appUserService.findById(id);
            appUserMapper.updateAppUserFromDto(userDTO, user);
            newId = appUserService.update(id, user);

        } catch (NotFoundException e) {
            return super.notFound(e);
        } catch (ValidationException e) {
            return super.badRequest(e);
        } catch (DataBaseException e) {
            return super.badRequest(e);
        }
        return super.ok(newId);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response> delete(@PathVariable("id") int id) {
        int newId = 0;
        try {
            newId = appUserService.delete(id);
        } catch (NotFoundException e) {
            return super.notFound(e);
        }
        return super.ok(newId);
    }
}
