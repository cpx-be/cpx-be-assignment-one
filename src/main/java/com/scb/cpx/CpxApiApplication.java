package com.scb.cpx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CpxApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CpxApiApplication.class, args);
	}

}
