package com.scb.cpx;

import com.scb.cpx.dao.AppUserDao;
import com.scb.cpx.model.AppUser;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Transactional
@SpringBootTest
public class AppUserDaoTest {
    @Autowired
    AppUserDao appUserDao;

    @Test
    public void canGetAllList() {

        if(appUserDao.list().isEmpty()) {
            List<AppUser> actualList = appUserDao.list();
            assertThat(actualList).isNotNull();
            assertThat(actualList.size()).isEqualTo(1);
        }
    }
}
