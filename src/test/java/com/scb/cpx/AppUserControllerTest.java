package com.scb.cpx;

import com.scb.cpx.controller.AppUserController;
import com.scb.cpx.model.AppUser;
import com.scb.cpx.service.AppUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@WebMvcTest(value = AppUserController.class)
public class AppUserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AppUserService appUserService;

    AppUser user = new AppUser( "User1", "Lastname1", "user1@gmail.com", "1", "001");

    String exampleUserJson = "{\n" +
            "    \"firstName\": \"Pornpan\",\n" +
            "    \"lastName\": \"Ampaporn\",\n" +
            "    \"email\": \"postpres@domain.com\",\n" +
            "    \"userType\": \"SCB\",\n" +
            "    \"employeeId\": \"1\"\n" +
            "}";

    @Test
    public void canFindUserById() throws Exception {

        Mockito.when(
                appUserService.findById(Mockito.anyInt())).thenReturn(user);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/app-users/1").accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        //System.out.println(result.getResponse());
        String expected = "{\n" +
                "    \"status\": 200,\n" +
                "    \"message\": \"The request has succeeded.\",\n" +
                "    \"data\": {\n" +
                "        \"id\": 1,\n" +
                "        \"createBy\": \"Admin\",\n" +
                "        \"createDate\": \"2021-04-22T00:00:00\",\n" +
                "        \"createByName\": null,\n" +
                "        \"modifyByName\": null,\n" +
                "        \"createByRole\": null,\n" +
                "        \"modifyByRole\": null,\n" +
                "        \"firstName\": \"Pornpan\",\n" +
                "        \"lastName\": \"Ampaporn\",\n" +
                "        \"email\": \"postpres@domain.com\",\n" +
                "        \"userType\": \"SCB\",\n" +
                "        \"employeeId\": \"1\"\n" +
                "    },\n" +
                "    \"result_error\": null\n" +
                "}";


        JSONAssert.assertEquals(expected, result.getResponse()
                .getContentAsString(), false);
    }

    @Test
    public void canCreateUser() throws Exception {
        AppUser user = new AppUser( "User2", "Lastname2", "user2@gmail.com", "1", "001");
        ;

        Mockito.when(appUserService.create(Mockito.any(AppUser.class))).thenReturn(1);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/app-users")
                .accept(MediaType.APPLICATION_JSON).content(exampleUserJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());


    }
}
