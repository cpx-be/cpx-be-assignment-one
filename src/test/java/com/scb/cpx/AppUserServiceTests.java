package com.scb.cpx;

import com.scb.cpx.dao.AppUserDao;
import com.scb.cpx.exception.DataBaseException;
import com.scb.cpx.exception.NotFoundException;
import com.scb.cpx.model.AppUser;
import com.scb.cpx.service.AppUserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class AppUserServiceTests {

    @Mock
    private AppUserDao appUserDao;

    @InjectMocks
    private AppUserServiceImpl appUserService;


    @Test
    void canCreateAppUser() throws NotFoundException, DataBaseException {
        AppUser user = new AppUser( "User1", "Lastname1", "user1@gmail.com", "1", "001");

        Integer id = appUserService.create(user);

        assertThat(id).isNotNull();

        //verify(appUserDao).create(any(AppUser.class));
        verify(appUserDao, times(1)).create(user);

    }
    @Test
    void canFindAppUserById() throws NotFoundException {
        final int id = 1;

        final AppUser user = new AppUser( null, "Lastname1", "user1@gmail.com", "1", "001");

        given(appUserDao.findById(id)).willReturn(user);

        final AppUser expected  =appUserService.findById(id);

        assertThat(expected).isNotNull();

    }
}
