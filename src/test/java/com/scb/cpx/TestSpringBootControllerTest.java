package com.scb.cpx;

import com.scb.cpx.controller.TestSpringBootController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = TestSpringBootController.class)
public class TestSpringBootControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void whenValidInput_thenReturns200()  {
        try {
            mockMvc.perform(MockMvcRequestBuilders.get("/be-hello"))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.data", is("Hello Backend Team")));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
